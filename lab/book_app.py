# -*- coding: utf-8 -*-
from flask import Flask, render_template
import sqlite3
from contextlib import closing
from flask import g

DATABASE = 'posts.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERNAME = 'admin'
PASSWORD = 'TajneHaslo'


#############
# Aplikacja #
#############

app = Flask(__name__)
app.config.from_object(__name__)

app.config['DEBUG'] = True

@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    #ksiazki = get_all_books()
    return render_template('book_list.html', ksiazki=ksiazki)


@app.route('/book/<book_id>/')
def book(book_id):
    book_info = get_book_info(book_id)
    if book_info != -1:
        return render_template('book_detail.html', ksiazka=book_info[0])
    else:
        return render_template('no_book.html')


@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    g.db.close()

def get_all_books():
    cur = g.db.execute('select id, title from books order by id desc')
    entries = [dict(id=row[0], title=row[1]) for row in cur.fetchall()]
    return entries

def get_book_info(id):
    cur = g.db.execute('SELECT title, isbn, author, publisher FROM books WHERE id=' + id)
    try:
        ksiazka = cur.fetchall()[0]
        book_info =[dict(title=ksiazka[0], isbn=ksiazka[1], author=ksiazka[2], publisher=ksiazka[3])]
        return book_info
    except:
        return -1

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def write_entry(title, isbn, author, publisher):
    with app.app_context():
        g.db.execute('insert into books (title, isbn, author, publisher) values (?, ?, ?, ?)', [title, isbn, author, publisher])
        g.db.commit()

def connect_db():
    return sqlite3.connect(DATABASE)

if __name__ == '__main__':
    init_db()
    app.run('localhost', 19991, debug=True)#194.29.175.240