# -*- coding: utf-8 -*-
from bottle import route, template, redirect, request, hook, static_file
import bottle
import sqlite3
import beaker.middleware
from contextlib import closing

session_opts = {
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.cookie_expires': 300,
    'session.auto': True,
}

app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)

USERNAME = 'admin'
PASSWORD = 'haslo12'

@hook('before_request')
def setup_request():
    request.session = request.environ['beaker.session']
    try:
        request.session['logged']
    except:
        request.session['logged'] = 0
    try:
        request.session['info']
    except:
        request.session['info'] = ''

@route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root='static')

@route('/')
def main():
    wpisy = wszystkie_wpisy()
    info = request.session['info']
    request.session['info'] = ''
    return template('main.html', wpisy=wpisy, logged=request.session['logged'], info=info)

@route('/login')
def login():
    return template('logowanie.html', msg=None)

@route('/login', 'POST')
def login():
    username = request.POST['username']
    password = request.POST['password']
    if username == USERNAME and password == PASSWORD:
        request.session['logged'] = 1
        request.session['info'] = u'Zostałeś zalogowany!'
        redirect('/')
    else:
        blad = u'Błędne dane!'
        return template('logowanie.html', msg=blad)

@route('/logout')
def logout():
    if request.session['logged'] == 1:
        request.session['logged'] = 0
        request.session['info'] = u'Zostałeś wylogowany!'
        redirect('/')
    else:
        blad = u'Aby się wylogować najpierw się zaloguj!'
        return template('logowanie.html', msg=blad)

@route('/add_post')
def add_post():
    if request.session['logged'] == 1:
        return template('dodawanie.html', msg=None)
    else:
        blad = u'Zaloguj się, aby dodać wpis!'
        return template('logowanie.html', msg=blad)

@route('/add_post', 'POST')
def add_post():
    tytul = request.POST['title']
    tresc = request.POST['text']
    if tytul.strip() == "" or tresc.strip() == "":
        blad = u'Wypełnij wszystkie pola!'
        return template('dodawanie.html', msg=blad)
    else:
        dodaj_wspis(tytul, tresc)
        request.session['info'] = u'Dodano wpis!'
        redirect('/')

@route('/delete/<id>')
def usun_post(id):
    if request.session['logged'] == 1:
        with closing(connect_db()) as db:
            db.execute('DELETE FROM posts WHERE id=' + id)
            db.commit()
        request.session['info'] = u'Usunięto wpis!'
        redirect('/')
    else:
        blad = u'Zaloguj się, aby usunąć wpis!'
        return template('logowanie.html', msg=blad)

def wszystkie_wpisy():
    with closing(connect_db()) as db:
        wpisy = db.execute('SELECT id, title, text FROM posts order by id desc')
        return [dict(id=row[0], title=row[1], text=row[2]) for row in wpisy]

def dodaj_wspis(tytul, tresc):
    with closing(connect_db()) as db:
        db.text_factory = str
        db.execute('INSERT INTO posts (title, text) VALUES(?, ?)', [tytul, tresc])
        db.commit()

def init_db():
    with closing(connect_db()) as db:
        with open('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def connect_db():
    return sqlite3.connect('posts.db')

if __name__ == '__main__':
    #run(app, host='localhost', port=19991)
    bottle.run(app=app, host='localhost', port=19991) #194.29.175.240