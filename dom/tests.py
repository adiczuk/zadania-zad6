# -*- coding: utf-8 -*-
import blog_app
import unittest
from webtest import TestApp


class BlogTestCase(unittest.TestCase):

    def setUp(self):
        self.app = TestApp(blog_app.app)
        blog_app.init_db()

    def test_db_is_empty(self):
        wpisy = blog_app.wszystkie_wpisy()
        self.assertEqual(len(wpisy), 0)

    def test_html_list_is_empty(self):
        response = self.app.get('/')
        assert u'Brak wpisów' in unicode(response.body, 'utf-8')

    def test_is_one_entry_in_db(self):
        blog_app.dodaj_wspis('Tytul1', 'Tresc1')
        wpisy = blog_app.wszystkie_wpisy()
        self.assertEqual(len(wpisy), 1)

    def test_is_one_entry_in_html_list(self):
        blog_app.dodaj_wspis('Tytul1', 'Tresc1')
        response = self.app.get('/')
        assert u'Brak wpisów' not in unicode(response.body, 'utf-8')

    def test_good_login(self):
        self.app.post('/login', {'username': 'admin', 'password': 'haslo12'}, status='*')
        response = self.app.get('/')
        assert u'Zostałeś zalogowany!' in unicode(response.body, 'utf-8')

    def test_bad_login(self):
        response = self.app.post('/login', {'username': 'admin', 'password': 'zlehaslo'}, status='*')
        assert u'Błędne dane!' in unicode(response.body, 'utf-8')

    def test_allow_to_add_post(self):
        self.app.post('/login', {'username': 'admin', 'password': 'haslo12'}, status='*')
        self.app.post('/add_post', {'title': 'Tytuł1', 'text': 'Tekst1'}, status='*')
        response = self.app.get('/')
        assert u'Dodano wpis!' in unicode(response.body, 'utf-8')

    def test_not_allow_to_add_post(self):
        self.app.get('/logout')
        response = self.app.get('/add_post')
        assert u'Zaloguj się, aby dodać wpis!' in unicode(response.body, 'utf-8')

    def test_logout_if_logged(self):
        self.app.post('/login', {'username': 'admin', 'password': 'haslo12'}, status='*')
        self.app.get('/logout')
        response = self.app.get('/')
        assert u'Zostałeś wylogowany!' in unicode(response.body, 'utf-8')

    def test_logout_if_not_logged(self):
        response = self.app.get('/logout')
        assert u'Aby się wylogować najpierw się zaloguj!' in unicode(response.body, 'utf-8')

if __name__ == '__main__':
    unittest.main()